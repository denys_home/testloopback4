import {Entity, model, property, belongsTo} from '@loopback/repository';
import {TodoList} from './todo-list.model';

@model({
    settings: {
        foreignKeys: {
            fk_todo_todoListId: {
                name: 'fk_todo_todoListId',
                entity: 'TodoList',
                entityKey: 'id',
                foreignKey: 'todolistid',
            },
        },
    },
})
export class Todo extends Entity {
    @property({
        type: 'number',
        id: true,
        generated: true,
    })
    id: number;

    @property({
        type: 'string',
        required: true,
    })
    title: string;

    @property({
        type: 'string',
    })
    desc?: string;

    @property({
        type: 'boolean',
    })
    isComplete?: boolean;

    @property({
        type: 'string'
    })
    task: string;

    @belongsTo(() => TodoList, {name: 'todoList'})
    todoListId: number;

    constructor(data?: Partial<Todo>) {
        super(data);
    }
}

export interface TodoRelations {
    // describe navigational properties here
}

export type TodoWithRelations = Todo & TodoRelations;
